package com.example.demo;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalculateRiskScoreTask implements JavaDelegate{

	private Logger LOG = LoggerFactory.getLogger(CalculateRiskScoreTask.class);
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		LOG.info("Calculation Risk Score");
		
		long userOverdraft= (long) execution.getVariable("User_Overdraft");
		long userCreditAmount= (long) execution.getVariable("Credit_Amount");
		long userCreditDuration= (long) execution.getVariable("Credit_Duration");
		
		
		execution.setVariable("denied", false);
		execution.setVariable("approuved", false);
		
		if ((userCreditAmount > 1000) && (userOverdraft > 0)) {
			execution.setVariable("approuved", true);
		}
		
		if ((userCreditDuration > 5) & (userCreditAmount < 1000)) {
			execution.setVariable("denied", true);
		}
	}
}
